import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

#Punto 1, definicio de funcion para el calculo de Pi
#basado en el método de cociente de áreas:
def Calculo_PI_1(N):
    puntos = np.random.uniform(low=0.0, high=1.0, size=(N,N))
    radios = (puntos[0]**2 + puntos[1]**2)**0.5
    events = np.count_nonzero(radios < 1)
    return 4*(events/N)

#Punto 2, definicion de funcion para el calculo de Pi
#basado en el método del valor medio
def Calculo_PI_2(N):
    x  = np.random.uniform(low=0.0, high=1.0, size=N)
    fx = (1 - x**2)**0.5
    return 4*np.mean(fx)

#Punto 5, definicio de funcion para el calculo de Pi
#basado en el metodo del buffon
def Calculo_PI_3(N, l=0.5, t= 1.0):
    a = np.random.uniform(0, np.pi, N)
    x = np.random.uniform(0, t / 2, N)
    
    k = np.sum(x <= l * np.sin(a) / 2)
    return 2 * l * N / (t * k)


#Resolucion de los puntos del ejercicio

#Punto 1: método de cociente de áreas
print("Punto 1: Valor de pi calculado por método de cociente de áreas para N = 1000")
print(Calculo_PI_1(1000),"\n")

#Punto 2: método del valor medio
print("Punto 2: Valor de pi calculado por método del valor medio para N = 1000")
print(Calculo_PI_2(1000),"\n")

#Punto 3: Grafica sobre una distribucion logaritmica
#Puntos espaciados logaritmicamente donde se realizara la simulacion
N = np.logspace(2, 4, num=20, endpoint=True, dtype=int)

#Vectorizacion de las funciones para facilitar el trabajo
VCalculo_PI_1 = np.vectorize(Calculo_PI_1)
VCalculo_PI_2 = np.vectorize(Calculo_PI_2)

#Calculo de valores de Pi para cada N
pi_1 = VCalculo_PI_1(N)
pi_2 = VCalculo_PI_2(N)

#Grafica de los metodos de forma conjunta
plt.figure(3)
plt.plot(N,pi_1, 'r', label="Método de cociente de áreas")
plt.plot(N,pi_2, 'b', label="Método del valor medio")
plt.legend()
plt.ylabel('Valores aproximados')
plt.xlabel("Valores de N, distribucion logaritmica")
plt.title("Punto 3: Comparacion de los primeros dos metodos")
#plt.show()

#Punto 4: Simulacion durante un numero v de veces utilizando n puntos
n = 15000
v = 1000

#Definicion de vector para aprovechar las funciones vectorizadas
N = np.ones(v,dtype=int)*n

#Grafica comparativa de los histograma generados por ambos metodos 
plt.figure(4)

#Data utilizando metodo de cociente de areas
data = VCalculo_PI_1(N)
#Calculo de media y desvio
mu, std = np.mean(data), np.std(data)
label = "Método de cociente de áreas"
plt.hist(data, bins=25, density=True, alpha=0.6, color='b', label = label)
#El histograma define los limites del grafico de la funcion normal
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'b', linewidth=2)

#Data utilizando metodo del valor medio
data = VCalculo_PI_2(N)
#Calculo de media y desvio
mu, std = np.mean(data), np.std(data)
label = "Método del valor medio"
plt.hist(data, bins=25, density=True, alpha=0.6, color='r', label = label)
#El histograma define los limites del grafico de la funcion normal
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'r', linewidth=2)

#Definicion de elementos comunes
plt.legend()
plt.ylabel('Densidad de Probabilidad')
plt.xlabel("Estimacion de Monte Carlo")
plt.title("Punto 4: Comparacion de Histograma")

#Punto 5: método del buffon
print("Punto 5: Valor de pi calculado por método del buffon para N = 1000")
print(Calculo_PI_3(1000),"\n")






exit()

#Opcional, grafica del primer metodo para explicar su funcionamiento
N = 1000
puntos = np.random.uniform(low=0.0, high=1.0, size=(N,N))
radios = (puntos[0]**2 + puntos[1]**2)**0.5
events = np.count_nonzero(radios < 1)
pi_cal = 4*(events/N)

x = np.linspace(0, 1, 100)
y = (1 - x**2)**0.5
plt.figure(100)
plt.plot(puntos[0],puntos[1], 'c.', label = "pi = {:}".format(pi_cal))
plt.plot(x,y, 'r--')
plt.legend()
plt.show()