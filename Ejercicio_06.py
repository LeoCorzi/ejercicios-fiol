#Damian Leonel Corzi

class Polinomio:
    "Clase para trabajar con polinomios"

    def __init__(self, lista = [0]):
        tipo = type(lista)
        if (tipo == list):
            self.lista = lista
        elif (tipo == int or tipo == float):
            self.lista = [lista]
        else :
            self.lista = [0]
        return None
    
    def grado(self):
        return len(self.lista)-1
    
    def get_coeficientes(self):
        return self.lista
    
    def set_coeficientes(self,lista):
        return self.__init__(lista)
    
    def suma_pol(self, P2):
        max_grado = max(self.lista,P2.lista)
        min_grado = min(self.lista,P2.lista)
        suma = [x+y for x,y in zip(max_grado,min_grado)]
        return Polinomio(suma + max_grado[len(min_grado):])
    
    def mul(self, k):
        return Polinomio([k*x for x in self.lista])
    
    def derivada(self, n=1):
        derivada = self.lista.copy()
        for u in range(n):
            derivada = [(i*coef) for i, coef in enumerate(derivada)][1:]
        
        if len(derivada) == 0:
            derivada = [0]
        return Polinomio(derivada)
    
    def integrada(self, n=1,cte=0):
        integrada = self.lista.copy()
        for u in range(n):
            integrada =[cte,integrada[0]] + [(coef/(i+1)) for i, coef in enumerate(integrada[1:],start=1)]
        return Polinomio(integrada)
    
    def eval(self, x):
        y = 0
        for i,coef in enumerate(self.lista):
            y += coef*x**i
        return y
    
    def __str__(self):
        texto = ""
    
    def __repr__(self):
        return self.__str__()

    def __call__(self,x):
        return self.eval(x)
    
    def __add__(self, P2):
        return self.suma_pol(P2)
    
    def __mul__(self, k):
        return self.mul(k)
    
    def __rmul__(self,k):
        return self.mul(k)
    
    
    
    
    
P1 = Polinomio([1,2,3])
P1.lista

P1.grado()
P1.get_coeficientes()
P1.set_coeficientes([1,2,3])

P2 = Polinomio([1,2,3,4,5])
C = P1.suma_pol(P2)
D = P1.mul(2)
E = P1.derivada(4)
F = P1.integrada(2)
G = P1.eval(2)
print(P1)
H = P1(2)
print(P1 + P1)
print(P1 * 2)
print(2 * P1)
print(P1.integrada(3).derivada(3))

