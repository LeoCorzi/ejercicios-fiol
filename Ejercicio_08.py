import argparse
import numpy as np
from scipy.optimize import fsolve

#Funcion caida libre
def caida_libre(t, h0, v0=0.0, g=9.8):
  """Devuelve la velocidad y la posición de una partícula en
  caída libre para condiciones iniciales dadas

  Parameters
  ----------
  t : float
      el tiempo al que queremos realizar el cálculo
  h0: float
      la altura inicial
  v0: float (opcional)
      la velocidad inicial (default = 0.0)
   g: float (opcional)
      valor de la aceleración de la gravedad (default = 9.8)

  Returns
  -------
  (v,h):  tuple of floats
       v= v0 - g*t
       h= h0 - v0*t -g*t^2/2

  """
  v = v0 - g*t
  h = h0 - v0*t - g*t**2/2
  return v,h


#       Analisis de los parametros pasados por comando

#Configuracion del parser
parser = argparse.ArgumentParser(description=caida_libre.__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

#Lista de argumentos esperados, tipo y valores por defecto
parser.add_argument('-v', '--velocidad', type=float, action="store", dest='v', default=0, help='Velocidad Inicial')
parser.add_argument('-a', '--altura', type=float, action="store", dest='a', default=1000, help='Altura inicial')
parser.add_argument('-g', '--grav', type=float, action="store", dest='g', default=9.8, help='Aceleracion')
parser.add_argument('-o', '--nombre', type=str, action="store", dest='o', default=None, help='Nombre de archivo para almacenar datos')
parser.add_argument('-N', '--ndatos', type=int, action="store", dest='N', default=100, help='Longitud del vector de datos')
parser.add_argument('-ti', '--instante_inicial', type=float, action="store", dest='ti', default=0, help='Instante inicial')
parser.add_argument('-tf', '--tiempo_final', type=float, action="store", dest='tf', default=None, help='Instante final')

#Variable que almacena los resultados
args = parser.parse_args()


#       Condiciones especificadas en el ejercicio

#El ejercicio especifica condiciones que fueron interpretadas de la siguiente manera:
#1) El tiempo inicial debe ser menor al tiempo que tarda el objeto en pasar por h = 0.
#2) El tiempo final por defecto debe ser igual al tiempo que tarda en pasar por h = 0.
#Debido a que el tiempo en que el objeto pasa por el punto h = 0, desde ahora TH0,
#depende de la velocidad inicial, la altura y la gravedad, sera calculado luego de
#contar con todos los parametros y se verificara que los valores ingresados en
#los tiempos inicial y final sean los correctos.

#Calculo del tiempo en el que h = 0 utilizando modulo scipy
TH0 = abs(fsolve(lambda t: args.a - args.v*t - args.g*t**2/2,14.3)[0])

#Condicion 1:
if TH0 < args.ti:
    raise TypeError("El tiempo inicial es mayor a TH0")

#Condicion 2:
if args.tf == None:
    args.tf = TH0

#Parametros de calculo
print("Parametros:")
print("-v  = {:}".format(args.v))
print("-a  = {:}".format(args.a))
print("-g  = {:}".format(args.g))
print("-o  = {:}".format(args.o))
print("-N  = {:}".format(args.N))
print("-ti = {:}".format(args.ti))
print("-tf = {:}".format(args.tf))
print("\n")


#       Procesamiento y almacenamiento

#Definicion del vector de tiempos
t = np.linspace(start=args.ti, stop=args.tf, num=args.N)

#Utilizacion de la funcion caida libre para calcular
#la velocidad y la altura para los tiempos indicados
v,h = caida_libre(t,args.a, args.v, args.g)

#Creacion de texto a almacenar o mostrar
texto = "time,\tspeed,\theight\n"
for i in range(args.N):
    texto += "{:.2f}".format(t[i]) + ",\t" + "{:.2f}".format(v[i]) + ",\t" + "{:.2f}".format(h[i]) + "\n"

#Si el nombre del archivo esta disponible, almacenar los datos
if type(args.o) == str :
    with open(args.o + ".csv", "w") as file:  
        file.write(texto)
        file.close()
else :
    print(texto)