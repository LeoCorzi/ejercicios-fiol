import argparse
import numpy as np

#Instante de tiempo cuando h = 0
TH0 = 1.25

#Funcion caida libre
def caida_libre(t, h0, v0=0.0, g=9.8):
  """Devuelve la velocidad y la posición de una partícula en
  caída libre para condiciones iniciales dadas

  Parameters
  ----------
  t : float
      el tiempo al que queremos realizar el cálculo
  h0: float
      la altura inicial
  v0: float (opcional)
      la velocidad inicial (default = 0.0)
   g: float (opcional)
      valor de la aceleración de la gravedad (default = 9.8)

  Returns
  -------
  (v,h):  tuple of floats
       v= v0 - g*t
       h= h0 - v0*t -g*t^2/2

  """
  v = v0 - g*t
  h = h0 - v0*t - g*t**2/2.
  return v,h

#Funcion que determina que el valor de Instante Inicial sea menor a TH0
class Rango:
    def __init__(self, start, end):
        self.start = start
        self.end = end
    def __eq__(self, other):
        return self.start <= other <= self.end 
    def __contains__(self, item):
        return self.__eq__(item)
    def __iter__(self):
        yield self
    def __repr__(self):
        return '[{:},{:}]'.format(self.start, self.end)

#Configuracion del parser
parser = argparse.ArgumentParser(description=caida_libre.__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

#Lista de argumentos esperados, tipo y valores por defecto
parser.add_argument('-v', '--velocidad', type=float, action="store", dest='v', default=0, help='Velocidad Inicial')
parser.add_argument('-a', '--altura', type=float, action="store", dest='a', default=1000, help='Altura inicial')
parser.add_argument('-g', '--grav', type=float, action="store", dest='g', default=9.8, help='Aceleracion')
parser.add_argument('-o', '--nombre', type=str, action="store", dest='o', default=None, help='Nombre de archivo para almacenar datos')
parser.add_argument('-N', '--ndatos', type=int, action="store", dest='N', default=100, help='Longitud del vector de datos')
parser.add_argument('-ti', '--instante_inicial', type=float, action="store", dest='ti', default=0, help='Instante inicial, debe ser menor a h = 0', choices=Rango(0.0,TH0))
parser.add_argument('-tf', '--tiempo_final', type=float, action="store", dest='tf', default=TH0, help='Instante final')

#Variable que almacena los resultados
args = parser.parse_args()



print(args.v,args.a,args.g,args.o,args.N,args.ti,args.tf)

exit()

#Definicion del vector de tiempos
t = np.linspace(start=args.ti, stop=args.tf, num=args.N)

#Utilizacion de la funcion caida libre para calcular
#la velocidad y la altura para los tiempos indicados
v,h = caida_libre(t,args.a, args.v, args.g)

#Creacion de texto a almacenar o mostrar
texto = ""
for i in range(args.N):
    texto += "{:.2f}".format(t[i]) + "\t" + "{:.2f}".format(v[i]) + "\t" + "{:.2f}".format(h[i]) + "\n"

if type(args.o) == str :
    with open(args.o + ".txt", "w") as file:  
        file.write(texto)
        file.close()
else :
    print(texto)