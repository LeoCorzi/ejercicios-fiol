# Ejercicios de Python

Ejercicios para entregar de la Materia Introducción al lenguaje Python para física e ingeniería del Instituto Balseiro.

## Ejercicio clase 2

Para la cadena de caracteres:

```python
s = '''Aquí me pongo a cantar
Al compás de la vigüela,
Que el hombre que lo desvela
Una pena estraordinaria
Como la ave solitaria
Con el cantar se consuela.'''
```

* Cuente la cantidad de veces que aparecen los substrings `es`, `la`, `que`, `co`, en los siguientes dos casos: distinguiendo entre mayúsculas y minúsculas, y no distinguiendo. Imprima el resultado.

* Cree una lista, donde cada elemento es una línea del string `s` y encuentre la de mayor longitud. Imprima por pantalla la línea y su longitud.

* Forme un nuevo string de 10 caracteres que contenga los 5 primeros y los 5 últimos del string anterior `s`. Imprima por pantalla el nuevo string.

* Forme un nuevo string que contenga los 10 caracteres centrales de `s` (utilizando un método que pueda aplicarse a otros strings también). Imprima por pantalla el nuevo string.

* Cambie todas las letras “m” por “n” y todas las letras “n” por “m” en `s`. Imprima el resultado por pantalla.

El programa al correrlo con el comando `python3` `Nombre_del_programa.py` debe imprimir:

```python
Nombre Apellido
Clase 2
Distinguiendo: 2 5 1 2
Sin distinguir: 2 5 2 4
Que el hombre que lo desvela : longitud=28
Aquí uela.
desvela
Un
Aquí ne pomgo a camtar
Al conpás de la vigüela,
Que el honbre que lo desvela
Uma pema estraordimaria
Cono la ave solitaria
Com el camtar se comsuela.
```

## Ejercicio clase 4

Describimos una grilla de sudoku como un string de nueve líneas, cada una con 9 números, con números entre 1 y 9. Escribir un conjunto de funciones que permitan chequear si una grilla de sudoku es correcta. Para que una grilla sea correcta deben cumplirse las siguientes condiciones.

* Los números están entre 1 y 9.
* En cada fila no deben repetirse.
* En cada columna no deben repetirse.
* En todas las regiones de 3x3 que no se solapan, empezando de cualquier esquina, no deben repetirse.

1- Escribir una función que convierta un string con formato a una lista bidimensional. El string estará dado con nueve números por línea, de la siguiente manera (los espacios en blanco en cada línea pueden variar):

```python
sudoku = """145327698
            839654127
            672918543
            496185372
            218473956
            753296481
            367542819
            984761235
            521839764"""
```

2- Escribir una función `check_repetidos()` que tome por argumento una lista (unidimensional) y devuelva verdadero si la lista tiene elementos repetidos y falso en caso contrario (puede ser conveniente explorar el uso de set).

3- Escribir la función `check_sudoku()` que toma como argumento una grilla (como una lista bidimensional de 9x9) y devuelva verdadero si los números corresponden a la resolución correcta del Sudoku y falso en caso contrario. Note que debe verificar que los números no se repiten en filas, ni en columnas ni en recuadros de 3x3. Para obtener la posición de los recuadros, puede investigar que hacen las líneas de código:

```python
j, k = (i // 3) * 3, (i % 3) * 3
r = [grid[a][b] for a in range(j, j+3) for b in range(k, k+3)]
```
suponiendo que `grid` es el nombre de nuestra lista bidimensional, cuando `i` toma valores entre `0` y `8`.

## Ejercicio clase 6

Cree una clase ``Polinomio`` para representar polinomios. La clase debe guardar los datos representando todos los coeficientes. El grado del polinomio será *menor o igual a 9* (un dígito). Se pide que programe:

* Un método de inicialización ``__init__`` que acepte una lista de coeficientes. Por ejemplo para el polinomio $`4 x^3 + 3 x^2 + 2 x + 1`$ usaríamos:

```python
>>> p = Polinomio([1,2,3,4])
```

* Un método ``grado`` que devuelva el orden del polinomio:

```python
>>> p = Polinomio([1,2,3,4])
>>> p.grado()
3
```

* Un método ``get_coeficientes``, que devuelva una lista con los coeficientes:

```python
>>> p.get_coeficientes()
[1, 2, 3, 4]
```

* Un método ``set_coeficientes``, que fije los coeficientes de la lista:

```python
>>> p1 = Polinomio()
>>> p1.set_coeficientes([1, 2, 3, 4])
>>> p1.get_coeficientes()
[1, 2, 3, 4]
```

* El método ``suma_pol`` que le sume otro polinomio y devuelva un polinomio (objeto del mismo tipo).

* El método ``mul`` que multiplica al polinomio por una constante y devuelve un nuevo polinomio.

* Un método, ``derivada``, que devuelva la derivada de orden ``n`` del polinomio (otro polinomio):

```python
>>> p1 = p.derivada()
>>> p1.get_coeficientes()
[2, 6, 12]
>>> p2 = p.derivada(n=2)
>>> p2.get_coeficientes()
[6, 24]
```

* Un método que devuelva la integral (antiderivada) del polinomio de orden ``n``, con constante de integración ``cte`` (otro polinomio).

```python
>>> p1 = p.integrada()
>>> p1.get_coeficientes()
[0, 1, 1, 1, 1]
>>> p2 = p.integrada(cte=2)
>>> p2.get_coeficientes()
[2, 1, 1, 1, 1]
>>> p3 = p.integrada(n=3, cte=1.5)
>>> p3.get_coeficientes()
[1.5, 1.5, 0.75, 0.16666666666666666, 0.08333333333333333, 0.05]
```

* El método ``eval``, que evalúe el polinomio en un dado valor de x.

```python
>>> p = Polinomio([1,2,3,4])
>>> p.eval(x=2)
49
>>> p.eval(x=0.5)
3.25
```

* Un método ``from_string`` (**si puede**) que crea un polinomio desde un string en la forma:

```python
>>> p = Polinomio()
>>> p.from_string('x^5 + 3x^3 - 2 x+x^2 + 3 - x')
>>> p.get_coeficientes()
[3, -3, 1, 3, 0, 1]
>>> p1 = Polinomio()
>>> p1.from_string('y^5 + 3y^3 - 2 y + y^2+3', var='y')
>>> p1.get_coeficientes()
[3, -2, 1, 3, 0, 1]
```

* Escriba un método llamado ``__str__``, que devuelva un string (que define cómo se va a imprimir el polinomio). Un ejemplo de salida será:

```python
>>> p = Polinomio([1,2.1,3,4])
>>> print(p)
4 x^3 + 3 x^2 + 2.1 x + 1
```

* Escriba un método llamado ``__call__``, de manera tal que al llamar al objeto, evalúe el polinomio (use el método ``eval`` definido anteriormente)

```python
>>> p = Polinomio([1,2,3,4])
>>> p(x=2)
49
>>>
>>> p(0.5)
3.25
```

* Escriba un método llamado ``__add__(self, p)``, que evalúe la suma de polinomios usando el método ``suma_pol`` definido anteriormente. Eso permitirá usar la operación de suma en la forma:

```python
>>> p1 = Polinomio([1,2,3,4])
>>> p2 = Polinomio([1,2,3,4])
>>> p1 + p2
```

* Escriba los métodos llamados ``__mul__(self, value)`` y ``__rmul__(self, value)``, que devuelvan el producto de un polinomio por un valor constante, llamando al método ``mul`` definido anteriormente. Eso permitirá usar la operación producto en la forma:

```python
>>> p1 = Polinomio([1,2,3,4])
>>> k = 3.5
>>> p1 * k
>>> k * p1
```

## Ejercicio clase 08

**Caída libre**: Cree un programa que calcule la posición y velocidad de una partícula en caída libre para condiciones iniciales dadas ($`h_{0}`$, $`v_{0}`$), y un valor de gravedad dados. Se utilizará la convención de que alturas y velocidades positivas corresponden a vectores apuntando hacia arriba (una velocidad positiva significa que la partícula se aleja de la tierra).

El programa debe realizar el cálculo de la velocidad y altura para un conjunto de tiempos equiespaciados. El usuario debe poder decidir o modificar el comportamiento del programa mediante opciones por línea de comandos.

El programa debe aceptar las siguientes opciones por líneas de comando:

* ``-v vel`` o, equivalentemente ``--velocidad=vel``, donde ``vel`` es el número dando la velocidad inicial en m/s. El valor por defecto será ``0``.

* ``-a alt`` o, equivalentemente ``--altura=alt``, donde ``alt`` es un número dando la altura inicial en metros. El valor por defecto será ``1000``. La altura inicial debe ser un número positivo.

* ``-g grav``, donde ``grav`` es el módulo del valor de la aceleración de la gravedad en $`m/s^2`$. El valor por defecto será ``9.8``.

* ``-o nombre`` o, equivalentemente ``--output=nombre``, donde ``nombre`` será el nombre de un archivo donde se escribirán los resultados. Si el usuario no usa esta opción, debe imprimir por pantalla (``sys.stdout``).

* ``-n N`` o, equivalentemente ``--Ndatos=N``, donde ``N`` es un número entero indicando la cantidad de datos que deben calcularse. Valor por defecto: ``100``.

* ``--ti=instante_inicial`` indica el tiempo inicial de cálculo. Valor por defecto: ``0``. No puede ser mayor que el tiempo de llegada a la posición $`h=0`$.

* ``--tf=tiempo_final`` indica el tiempo inicial de cálculo. Valor por defecto será el correspondiente al tiempo de llegada a la posición $`h=0`$.

## Ejercicio clase 10

Estimar el valor de π usando diferentes métodos basados en el método de Monte Carlo:

1. Crear una función para calcular el valor de $`\pi`$ usando el “método de cociente de áreas”. Para ello:

* Generar puntos en el plano dentro del cuadrado de lado unidad cuyo lado inferior va de $`x=0`$ a $`x=1`$.
* Contar cuantos puntos caen dentro del (cuarto de) círculo unidad. Este número tiende a ser proporcional al área del círculo.
* La estimación de $`\pi`$ será igual a cuatro veces el cociente de números dentro del círculo dividido por el número total de puntos.

2. Crear una función para calcular el valor de $`\pi`$ usando el “método del valor medio”: Este método se basa en la idea de que el valor medio de una función se puede calcular de la siguiente manera:

```math
\langle f \rangle = \frac{1}{b-a} \int_{a}^{b} f(x)\, dx 
```
Tomando la función particular $`f(x)= \sqrt{1- x^{2}}`$ entre $`x=0`$ y $`x=1`$, obtenemos:

```math
\langle f \rangle = \int_{0}^{1} \sqrt{1- x^{2}}\, dx = \frac{\pi}{4} 
```

Entonces, tenemos que estimar el valor medio de la función $`f`$ y, mediante la relación anterior obtener $`\pi = 4 \langle f(x) \rangle`$. Para obtener el valor medio de la función notamos que si tomamos $`X`$ es una variable aleatoria entre 0 y 1, entonces el valor medio de $`f(X)`$ es justamente $`\langle f \rangle`$. Su función debe entonces

* Generar puntos aleatoriamente en el intervalo $`[0,1]`$
* Calcular el valor medio de $`f(x)`$ para los puntos aleatorios $`x`$.
* El resultado va a ser igual al valor de la integral, y por lo tanto a $`\pi/4`$.

3. Utilizar las funciones anteriores con diferentes valores para el número total de puntos $`N`$. En particular, hacerlo para 20 valores de $`N`$ equiespaciados logarítmicamente entre 100 y 10000. Para cada valor de $`N`$ calcular la estimación de $`pi`$. Realizar un gráfico con el valor estimado como función del número $`N`$ con los dos métodos (dos curvas en un solo gráfico).

4. Para $`N=15000`$ repetir el “experimento” muchas veces (al menos 1000) y realizar un histograma de los valores obtenidos para $`\pi`$ con cada método. Graficar el histograma y calcular la desviación standard. Superponer una función Gaussiana con el mismo ancho. El gráfico debe ser similar al siguiente (*el estilo de graficación no tiene que ser el mismo*).

![](https://fiolj.github.io/intro-python-IB/_images/ejercicio_09_1.png)

5. El método de la aguja del bufón se puede utilizar para estimar el valor de $`\pi`$, y consiste en tirar agujas (o palitos, fósforos, etc) al azar sobre una superficie rayada.

![](https://fiolj.github.io/intro-python-IB/_images/Streicholz-Pi-wiki.jpg)

Por simplicidad vamos a considerar que la distancia entre rayas $`t`$ es mayor que la longitud de las agujas $`\ell`$.

![](https://fiolj.github.io/intro-python-IB/_images/Buffon_needle_wiki.png)

La probabilidad de que una aguja cruce una línea será:

```math
P = \frac{2 \ell}{t\, \pi} 
```
por lo que podemos calcular el valor de $`\pi`$ si estimamos la probabilidad $`P`$. Realizar una función que estime $`\pi`$ utilizando este método y repetir las comparaciones de los dos puntos anteriores pero ahora utilizando este método y el de las áreas.

## Ejercicio clase 14

**PARA ENTREGAR. Caída libre 2:** Modificar el ejercicio de la clase 8 de caída libre que entregó, para aceptar dos nuevas opciones:

* La opción ``--vx`` permite dar una velocidad inicial en la dirección horizontal.
* La opción ``--animate``, tal que cuando se utilice, el programa muestre una animación de la trayectoria.
* La animación tiene que tener un cartel indicando el tiempo, y la velocidad y altura correspondiente a ese tiempo.
* Agregue una “cola fantasma” a la partícula, que muestre posiciones anteriores.