#Autor: Damian Leonel Corzi

sudoku = """145327698
839654127
672918543
496185372
218473956
753296481
367542819
984761235
521839764"""

#string con formato a una lista bidimensional
grid = [ list(x) for x in sudoku.splitlines() ]

#funcion check_repetidos
def check_repetidos(lista):
    """
    Verifica que no haya elementos repetidos
    en una lista de cualquier longitud,
    devuelve True en caso de encontrar repetidos.
    """
    return len(set(lista)) != len(lista)

#Funcion que divide en recuadros
def split_recuadros(grid,i):
    """
    Entrega una lista de 3x3 a partir de una lista
    bidimesional cuadrada segun el indice i.
    No checkea dimesiones de lista de entrada.
    """
    j, k = (i // 3) * 3, (i % 3) * 3
    return [grid[a][b] for a in range(j, j+3) for b in range(k, k+3)]

#Funcion que divide en columnas
def split_columnas(grid,i):
    """
    Entrega la columna i de una lista bidimensional
    """
    return [row[i] for row in grid]

#funcion check_sudoku
def check_sudoku(grid):
    """
    Checkea si un sudoku es correcto
    """
    for i in range(len(grid)):
        if check_repetidos(grid[i]) or check_repetidos(split_columnas(grid,i)) or check_repetidos(split_recuadros(grid,i)) :
            return True
    return False

print("El sudoku es incorrecto?", check_sudoku(grid))