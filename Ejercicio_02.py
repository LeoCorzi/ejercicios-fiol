print('Leo Corzi')
print('Clase 2')

s = '''Aquí me pongo a cantar
Al compás de la vigüela,
Que el hombre que lo desvela
Una pena estraordinaria
Como la ave solitaria
Con el cantar se consuela.'''

print('Distinguiendo:', s.count('es'), s.count('la'), s.count('que'), s.count('co'))

S = s.upper()
print('Sin distinguir:', S.count('ES'), S.count('LA'), S.count('QUE'), S.count('CO'))

lineas = s.split('\n')
x = max(lineas, key=len)
print(x, ': longitud=', len(x))

print(s[:5]+s[-5:])

n = len(s)//2
L = s[(n-5):(n+5)]
print(L)

b = s.replace('m','1').replace('n','m').replace('1','n')
print(b)
exit()